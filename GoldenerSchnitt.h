#ifndef GOLDENERSCHNITT_H
#define GOLDENERSCHNITT_H

#include <math.h>
#include <iostream>

class GoldenerSchnitt {
public:
	static void berechnungGSAB(double, double, int, int);
private:
	static double berechneGamma();
};

#endif