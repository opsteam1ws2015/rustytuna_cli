/*
	Autor: Andreas Gosch, Philip Klaus
	Datum: 17.10.2015
	Beschreibung: Implementierung des Goldenen Schnitts lt. Uchida Skript
*/

#include "GoldenerSchnitt.h"
#include "FunctionContainer.h"

void GoldenerSchnitt::berechnungGSAB(double a, double b, int iterationen, int index)
{
	std::cout << "| Verfahren: Goldener Schnitt |\n -------------------------------- \n\n";
	std::cout << "Parameter:\n- Untergrenze (A) = " << a << "\n- Obergrenze (B) = " << b << "\n- Iterationen = " << iterationen << std::endl << std::endl;
	std::cout << "Berechnung:\n";
	double lamda, my;
	
	for (int i = 0; i < iterationen; ++i) {		
		lamda = a + (1 - berechneGamma()) * (b - a);
		my = a + berechneGamma() * (b - a);
		if (FunctionContainer::getFunction(lamda, index) < FunctionContainer::getFunction(my, index)) {
			b = my;
		}
		else {
			a = lamda;
		}
		std::cout << "Iteration " << i << " A: " << a << " B: " << b << std::endl;
	}
	std::cout << "\nDas Minimum liegt im Intervall (x): [" << a << "," << b << "]\nund y liegt im Intervall (y): [" << FunctionContainer::getFunction(a, index) << ", " << FunctionContainer::getFunction(b, index) << "]\n";
}

double GoldenerSchnitt::berechneGamma()
{
	return (sqrt(5) - 1) / 2;
}
