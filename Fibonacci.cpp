/*
	Autor: Andreas Gosch, Philip Klaus
	Datum: 17.10.2015
	Beschreibung: Implementierung des Fibbonacci Algorithmus
*/

#include "Fibonacci.h"
#include "FunctionContainer.h"

void Fibonacci::minFibonacci(double grenzeU, double grenzeO, unsigned n, int functionIndex) {
	std::cout << "| Verfahren: Fibonacciverfahren |\n ------------------------------- \n\n";
	std::cout << "Parameter:\n- Untergrenze = " << grenzeU << "\n- Obergrenze = " << grenzeO << "\n- Iterationen = " << n << std::endl << std::endl;
	std::cout << "Berechnung:\n";
	double a = grenzeU;
	double b = grenzeO;
	double x1 = 0, x2 = 0, y1 = 0, y2 = 0;
	for (unsigned k = 1; k < n; k++) {
		x1 = a + (b - a)*(fibonacci(n - k - 1) / fibonacci(n - k + 1));
		x2 = a + (b - a)*(fibonacci(n - k) / fibonacci(n - k + 1));
		//------- Berechnen der Funktionswerte --------
		y1 = FunctionContainer::getFunction(x1, functionIndex);//pow((x1 - 30), 2);
		y2 = FunctionContainer::getFunction(x2, functionIndex);///pow((x2 - 30), 2);
		//------- Ausgabe der derzeitigen Daten -------
		std::cout << std::fixed << std::setprecision(5) << "It.: " << k << "\tx1: " << x1 << "\tx2: " << x2 << "\ty1: " << y1 << "\ty2: " << y2 << "\t\nInterv. [ " << a << ", " << b << " ]\n";
		//------- Setzen des neuen Intervalls ---------
		if (y1 < y2)
			b = x2;
		else
			a = x1;
	}
	std::cout << "\nDas Minimum liegt im Intervall (x): [" << x1 << "," << x2 << "].\nDer Wert des Minimums liegt im Intervall (y): [" << y1 << "," << y2 << "]\n\n";
}

double Fibonacci::fibonacci(unsigned index) {
	double fibOld1 = 1, fibOld2 = 1, fibCurr = 1;
	for (unsigned i = 2; i <= index; i++) {
		fibCurr = fibOld1 + fibOld2;
		fibOld2 = fibOld1;
		fibOld1 = fibCurr;
	}
	return fibCurr;
}