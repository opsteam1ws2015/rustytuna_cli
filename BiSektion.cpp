/*
	Autor: Andreas Gosch, Philip Klaus
	Datum: 17.10.2015
	Beschreibung: Implementierung des gefundenen BiSektions-Verfahren zum ermitteln des lokalen Minimums
*/

#include "BiSektion.h"
#include "FunctionContainer.h"

void BiSektion::nullBisektion(double grenzeU, double grenzeO, double genauigkeit, int functionIndex) {
	std::cout << "| Verfahren: Bisektionsverfahren |\n -------------------------------- \n\n";
	std::cout << "Parameter:\n- Untergrenze = " << grenzeU << "\n- Obergrenze = " << grenzeO << "\n- Genauigkeit = " << genauigkeit << std::endl<<std::endl;
	std::cout << "Berechnung:\n";
	double t = genauigkeit;
	double a = grenzeU;
	double b = grenzeO;
	double xn,ya,yb;
	double yn = 1;
	
	while (yn < 0-genauigkeit || yn > 0 + genauigkeit) {
		xn = (a + b) / 2;
		yn = FunctionContainer::getDerivatedFunction(xn, functionIndex);// 2 * (xn + 2);// pow(xn, 2) - 1;
		ya = FunctionContainer::getDerivatedFunction(a, functionIndex); //2 * (a + 2);//pow(a, 2) - 1;
		yb = FunctionContainer::getDerivatedFunction(b, functionIndex); //2 * (b + 2);//pow(b, 2) - 1;
		std::cout << std::fixed << std::setprecision(5) << "a: " << a << "\tb: " << b << "\txn: " << xn << "\tyn: " << yn << std::endl;
		
		if (ya < yb) {
			if (yn < 0)
				a = xn;
			else if (yn > 0)
				b = xn;
		}
		else if (ya > yb) {
			if (yn < 0)
				b = xn;
			else if (yn > 0)
				a = xn;
		}
	}
	std::cout << "\nDas Minimum liegt bei x: '" << xn << "' und hat den Wert y: '" << FunctionContainer::getFunction(xn,functionIndex) << "'\n\n";
}