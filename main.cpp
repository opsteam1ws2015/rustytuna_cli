/*
	Autor: Andreas Gosch & Philip Klaus
	Datum: 1.1.2016
*/
 
#include <iostream>
#include <stdlib.h> 
#include "BiSektion.h"
#include "Fibonacci.h"
#include "GoldenerSchnitt.h"

using namespace std;
int main(){
	char input='j';
	int verfahren, funktion, iterationen;
	double obergrenze, untergrenze, genauigkeit;
	
	cout << " OPS Team 1 \n------------\n\nThema: Nichtlineare Optimierung ohne NB-Eindimensional\n\n" << endl;
	do {
		cout << "Waehlen Sie ein Optimierungsverfahren durch Eingabe der Nummer aus:\n";
		cout << "(0) Fibonacciverfahren\n(1) Goldener Schnitt\n(2) Bisektionsverfahren";
		do {
			cout << "\nIhre Auswahl: ";
			cin >> verfahren;
		} while(verfahren > 2 ||verfahren < 0);
		cout << "\nWaehlen Sie eine Funktion durch Eingabe der Nummer aus:\n";
		cout << "(0) f(x) = (x-30)^2\n(1) f(x) = x^5+5*x^4+5*x^3-5*x^2-6*x\n(2) f(x) = x^4-16*x^2-1\n(3) f(x) = (x^4/4)-x^2+2*x\n(4) f(x) = 2*x^2+e^(-2*x)" ;
		do {
			cout << "\nIhre Auswahl: ";
			cin >> funktion;
		} while(funktion > 4 || funktion < 0);
		
		cout << "\nGeben Sie die Untergrenze ein: ";
		cin >> untergrenze;
		cout << "Geben Sie die Obergrenze ein:";
		cin >> obergrenze;
		if(verfahren == 0 || verfahren == 1) {
			cout << "Geben Sie die Anzahl an Iterationen ein: ";
			cin >> iterationen;
		}
		else {
			cout << "Geben Sie die Genauigkeit ein: ";
			cin >> genauigkeit;
		}
		
		switch(verfahren) {
			case 0:
				Fibonacci::minFibonacci(untergrenze, obergrenze, iterationen, funktion);
			break;
			case 1:
				GoldenerSchnitt::berechnungGSAB(untergrenze, obergrenze, iterationen, funktion);
			break;
			case 2:
				BiSektion::nullBisektion(untergrenze, obergrenze, genauigkeit, funktion);
			break;
		}
		cout << "\n\nWollen Sie noch eine Berechnung durchfuehren ('j'-ja | 'n'-nein): ";
		cin >> input;
	} while(input == 'j');
	
	system("pause");
	return 0;
}