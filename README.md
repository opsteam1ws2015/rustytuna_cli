# OPS Projekt WS2015 - CLI Version #

## Befehl zum kompilieren ##
    g++ BiSektion.cpp Fibonacci.cpp FunctionContainer.cpp Funktion.cpp GoldenerSchnitt.cpp main.cpp -o rustytuna.exe -fno-exceptions

Die Endung *.exe kann auf Linux bzw. Unixbasierten Betriebssystemen logischerweise weggelassen werden.

Das Programm besteht im ganzen aus drei Komponenten:

* Function Container - Dieser enthält die zu berechnenten Funktionen und ihre Ableitungen
* Berechnungsklassen - Für jedes Verfahren wurde eine eigene Klasse angelegt
* CLI - Die kommandozeilenbasierte Oberfläche dient zum Eingeben der Daten. Das Ergebnis wird im selben Konsolenfenster ausgegeben.